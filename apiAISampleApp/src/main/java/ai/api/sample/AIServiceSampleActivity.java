/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ai.api.sample;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import ai.api.android.AIConfiguration;
import ai.api.AIListener;
import ai.api.android.AIService;
import ai.api.android.GsonFactory;
import ai.api.model.AIError;
import ai.api.model.AIResponse;
import ai.api.model.Metadata;
import ai.api.model.Result;
import ai.api.model.Status;
import ai.api.sample.object.Person;
import ai.api.sample.object.ResponseObject;

public class AIServiceSampleActivity extends BaseActivity implements AIListener {

    public static final String TAG = AIServiceSampleActivity.class.getName();
    private static List<Person> people = new ArrayList<>();

    static {
        people.add(new Person("Adam", "Swift                                                           ", "20", "penicillin"));
        people.add(new Person("John", "Smith", "24", "penicillin", "nuts"));
        people.add(new Person("Andrew", "Smith", "28", "kiwis", "ibalgin", "codeine"));
    }

    private AIService aiService;
    private ProgressBar progressBar;
    private ImageView recIndicator;
    private boolean recording = false;

    private Gson gson = GsonFactory.getGson();

    private String listeningFor = "";
    private List<String> problems = new ArrayList<>();
    static private Map<String, JsonElement> loadedParams = new HashMap<>();

    private ImageView startExaminationButton;
    private LinearLayout startPageLayout;
    private LinearLayout examinationPageLayout;

    private ImageView micButton;
    private ImageView endButton;
    private ImageView endExButton;

    private TextView firstname;
    private TextView lastname;
    private TextView symptoms;
    private TextView examination;
    private TextView diagnosis;
    private TextView drugs;
    private TextView age;
    private TextView allergies;
    private Person chosenPerson;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_main);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recIndicator = (ImageView) findViewById(R.id.recIndicator);

        startPageLayout = (LinearLayout) findViewById(R.id.start_page_layout);
        startExaminationButton = (ImageView) findViewById(R.id.button_start_examination);
        examinationPageLayout = (LinearLayout) findViewById(R.id.examination_page_layout);
        micButton = (ImageView) findViewById(R.id.mic_button);
        endButton = (ImageView) findViewById(R.id.end_button);

        firstname = (TextView) findViewById(R.id.firstnameValue);
        lastname = (TextView) findViewById(R.id.lastnameValue);
        symptoms = (TextView) findViewById(R.id.symptomsValue);
        examination = (TextView) findViewById(R.id.examinationValue);
        diagnosis = (TextView) findViewById(R.id.diagnosisValue);
        drugs = (TextView) findViewById(R.id.drugsValue);
        age = (TextView) findViewById(R.id.ageValue);
        allergies = ((TextView) findViewById(R.id.allergiesValue));

        endExButton = (ImageView) findViewById(R.id.button_end_examination);

        initService(Config.languages[0]);

        startExaminationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleRecognition(v);
                examinationPageLayout.setVisibility(View.VISIBLE);
                startPageLayout.setVisibility(View.GONE);

            }
        });

        micButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleRecognition(v);
            }
        });

        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecognition(v);
                examinationPageLayout.setVisibility(View.GONE);
                startPageLayout.setVisibility(View.VISIBLE);
                firstname.setText("");
                lastname.setText("");
                symptoms.setText("");
                examination.setText("");
                diagnosis.setText("");
                drugs.setText("");
                age.setText("");
                loadedParams.clear();
                allergies.setText("");
            }
        });


        endExButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                examinationPageLayout.setVisibility(View.GONE);
                startPageLayout.setVisibility(View.VISIBLE);
                firstname.setText("");
                lastname.setText("");
                symptoms.setText("");
                examination.setText("");
                diagnosis.setText("");
                drugs.setText("");
                age.setText("");
                loadedParams.clear();
                allergies.setText("");
            }
        });

        TTS.init(getApplicationContext());

    }

    private void initService(final LanguageConfig selectedLanguage) {
        final AIConfiguration.SupportedLanguages lang = AIConfiguration.SupportedLanguages.fromLanguageTag(selectedLanguage.getLanguageCode());
        final AIConfiguration config = new AIConfiguration(selectedLanguage.getAccessToken(),
                lang,
                AIConfiguration.RecognitionEngine.System);

        if (aiService != null) {
            aiService.pause();
        }

        aiService = AIService.getService(this, config);
        aiService.setListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_aiservice_sample, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        final int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(AISettingsActivity.class);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // use this method to disconnect from speech recognition service
        // Not destroying the SpeechRecognition object in onPause method would block other apps from using SpeechRecognition service
        if (aiService != null) {
            aiService.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // use this method to reinit connection to recognition service
        if (aiService != null) {
            aiService.resume();
        }
    }

    public void toggleRecognition(final View view) {
        if (recording) {
            cancelRecognition(view);
        } else {
            startRecognition(view);
        }

        recording = !recording;
    }

    public void startRecognition(final View view) {
        aiService.startListening();
    }

    public void stopRecognition(final View view) {
        aiService.stopListening();
        recording = false;
    }

    public void cancelRecognition(final View view) {
        aiService.cancel();
    }

    @Override
    public void onResult(final AIResponse response) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "onResult");

                startRecognition(findViewById(android.R.id.content));

                checkEnd();

                TextView problemsTextView = (TextView) findViewById(R.id.temperature);
                if (response.getResult().getParameters() != null) {

                    if (response.getResult().getParameters().containsKey("temperature")) {
                        ResponseObject responseObject = gson.fromJson(response.getResult().getParameters().get("temperature"), ResponseObject.class);
                        problemsTextView.setText(responseObject.getAmount());
                    }
                }

                Log.i(TAG, gson.toJson(response));
                Log.i(TAG, "Received success response");

                // this is example how to get different parts of result object
                final Status status = response.getStatus();
                Log.i(TAG, "Status code: " + status.getCode());
                Log.i(TAG, "Status type: " + status.getErrorType());

                final Result result = response.getResult();
                Log.i(TAG, "Resolved query: " + result.getResolvedQuery());

                Log.i(TAG, "Action: " + result.getAction());

                final String speech = result.getFulfillment().getSpeech();
                Log.i(TAG, "Speech: " + speech);
                TTS.speak(speech);

                final Metadata metadata = result.getMetadata();
                if (metadata != null) {
                    Log.i(TAG, "Intent id: " + metadata.getIntentId());
                    Log.i(TAG, "Intent name: " + metadata.getIntentName());
                }

                final HashMap<String, JsonElement> params = result.getParameters();
                if (params != null && !params.isEmpty()) {
                    Log.i(TAG, "Parameters: ");
                    for (final Map.Entry<String, JsonElement> entry : params.entrySet()) {
                        Log.i(TAG, String.format("%s: %s", entry.getKey(), entry.getValue().toString()));
                    }
                }
                updateMedicalRecords(params);
            }

        });
    }

    private void checkEnd() {
        if (!Objects.equals(firstname.getText(), "")) {
            if (!Objects.equals(lastname.getText(), "")) {
                if (!Objects.equals(symptoms.getText(), "")) {
                    if (!Objects.equals(examination.getText(), "")) {
                        if (!Objects.equals(diagnosis.getText(), "")) {
                            if (!Objects.equals(drugs.getText(), "")) {
                                endExButton.setVisibility(View.VISIBLE);
                                stopRecognition(findViewById(android.R.id.content));
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateMedicalRecords(Map<String, JsonElement> params) {
        if (params != null) {
            for (Map.Entry<String, JsonElement> it : params.entrySet()) {
                if (loadedParams.containsKey(it.getKey()) && isArrayType(it.getKey())) {
                    Iterator<JsonElement> jsonIt = ((JsonArray) it.getValue()).iterator();
                    while (jsonIt.hasNext()) {
                        ((JsonArray) loadedParams.get(it.getKey())).add(jsonIt.next());
                    }
                } else {
                    loadedParams.put(it.getKey(), it.getValue());
                }
            }
            setMedicalRecord(R.id.firstnameValue, "given-name", loadedParams);
            setMedicalRecord(R.id.lastnameValue, "last-name", loadedParams);
            findPerson(loadedParams);
            setMedicalRecord(R.id.ageValue, "Age", loadedParams);
            setMedicalRecordArray(R.id.symptomsValue, "Illnesses", loadedParams);
            setMedicalRecordArray(R.id.examinationValue, "Examination", loadedParams);
            setMedicalRecord(R.id.temperatureValue, "temperature", loadedParams); // @TODO test
            setMedicalRecord(R.id.pulseValue, "Pulse", loadedParams); //@TODO test
            setMedicalRecord(R.id.drugsValue, "Drugs", loadedParams);
            if (chosenPerson != null
                    && loadedParams.containsKey("Drugs")
                    && chosenPerson.isAllergicTo(loadedParams.get("Drugs").getAsString())) {
                ((TextView) findViewById(R.id.drugsValue)).setTextColor(Color.RED);
            } else {
                ((TextView) findViewById(R.id.drugsValue)).setTextColor(Color.BLACK);
            }
            setMedicalRecord(R.id.diagnosisValue, "Diagnosis", loadedParams);
        }
    }

    private boolean isArrayType(String key) {
        key = key.toLowerCase();
        List<String> arrayTypes = Arrays.asList("illnesses", "examination");
        for (int i = 0; i < arrayTypes.size(); i++) {
            if (arrayTypes.get(i).toLowerCase().contains(key)) {
                return true;
            }
        }
        return false;
    }

    private void setMedicalRecord(int valueId, String key, Map<String, JsonElement> map) {
        if (!map.containsKey(key)) {
            Log.i(TAG, key + " isn't loaded");
            return;
        }

        String text = map.get(key).toString().replaceAll("\"", "");
        Log.i(TAG, key + " is loaded.");
        TextView value = (TextView) findViewById(valueId);
        value.setText(text);
    }

    private void setMedicalRecordArray(int valueID, String key, Map<String, JsonElement> map) {
        if (!map.containsKey(key)) {
            Log.i(TAG, key + " isn't loaded");
            return;
        }
        Log.i(TAG, key + " is loaded.");

        StringBuilder textBuilder = new StringBuilder();
        Iterator<JsonElement> it = map.get(key).getAsJsonArray().iterator();
        while (it.hasNext()) {
            JsonElement elem = it.next();
            textBuilder.append(elem.toString().replaceAll("\"", ""));
            if (it.hasNext()) {
                textBuilder.append("\n");
            }
        }

        TextView value = ((TextView) findViewById(valueID));
        value.setText(textBuilder.toString());
    }

    private void findPerson(Map<String, JsonElement> params) {
        String firstName = ((TextView) findViewById(R.id.firstnameValue)).getText().toString();
        String lastName = ((TextView) findViewById(R.id.lastnameValue)).getText().toString();
        String name = (firstName + " " + lastName).toLowerCase().trim();
        if (name.equals("")) {
            return;
        }
        for (Person person : people) {
            String personName = (person.name + " " + person.surname).toLowerCase();
            if (personName.contains(name)) {
                choosePerson(person, params);
                return;
            }
        }
    }

    private void choosePerson(Person person, Map<String, JsonElement> params) {
        JsonObject ageJson = new JsonObject();
        ageJson.addProperty("Age", person.age);
        ageJson.addProperty("given-name", person.name);
        ageJson.addProperty("last-name", person.surname);
        params.put("Age", ageJson.get("Age"));
        params.put("given-name", ageJson.get("given-name"));
        params.put("last-name", ageJson.get("last-name"));
        setMedicalRecord(R.id.firstnameValue, "given-name", loadedParams);
        setMedicalRecord(R.id.lastnameValue, "last-name", loadedParams);
        loadAllergies(person);
        chosenPerson = person;
    }

    private void loadAllergies(Person person) {
        ((TextView) findViewById(R.id.allergiesValue)).setText(joinStrings(person.allergies, "\n"));
    }

    @Override
    public void onError(final AIError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAudioLevel(final float level) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float positiveLevel = Math.abs(level);

                if (positiveLevel > 100) {
                    positiveLevel = 100;
                }
                progressBar.setProgress((int) positiveLevel);
            }
        });
    }

    @Override
    public void onListeningStarted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recIndicator.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onListeningCanceled() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recIndicator.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onListeningFinished() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recIndicator.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void startActivity(Class<?> cls) {
        final Intent intent = new Intent(this, cls);
        startActivity(intent);
    }

    public static String joinStrings(List<String> strings, String delimeter) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < strings.size(); i++) {
            builder.append(strings.get(i));
            if (i < (strings.size() - 1)) {
                builder.append(delimeter);
            }
        }
        return builder.toString();
    }
}
