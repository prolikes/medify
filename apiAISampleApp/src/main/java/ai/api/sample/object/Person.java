package ai.api.sample.object;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Person {
    public String name, surname, age;
    public List<String> allergies = new ArrayList<>();

    public Person(String name, String surname, String age, String... alergies) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.allergies.addAll(Arrays.asList(alergies));
    }

    public boolean isAllergicTo(String allergy) {
        return allergies.contains(allergy.toLowerCase().trim());
    }
}
